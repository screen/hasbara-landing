/* CUSTOM ON LOAD FUNCTIONS */
function hasbaraCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    const testimonialsSwiper = new Swiper('.swiper-testimonials', {
        direction: 'horizontal',
        loop: true,
        slidesPerView: 1,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    });
}

document.addEventListener("DOMContentLoaded", hasbaraCustomLoad, false);