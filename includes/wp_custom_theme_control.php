<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'hasbara_customize_register');

function hasbara_customize_register($wp_customize)
{

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('hsb_social_settings', array(
        'title'    => __('Redes Sociales', 'hasbara'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'hasbara'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('hsb_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'hasbara_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'hsb_social_settings',
        'settings' => 'hsb_social_settings[facebook]',
        'label' => __('Facebook', 'hasbara'),
    ));

    $wp_customize->add_setting('hsb_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'hasbara_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'hsb_social_settings',
        'settings' => 'hsb_social_settings[twitter]',
        'label' => __('Twitter', 'hasbara'),
    ));

    $wp_customize->add_setting('hsb_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'hasbara_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'hsb_social_settings',
        'settings' => 'hsb_social_settings[instagram]',
        'label' => __('Instagram', 'hasbara'),
    ));

    $wp_customize->add_setting('hsb_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'hasbara_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'hsb_social_settings',
        'settings' => 'hsb_social_settings[linkedin]',
        'label' => __('LinkedIn', 'hasbara'),
    ));

    $wp_customize->add_setting('hsb_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'hasbara_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'hsb_social_settings',
        'settings' => 'hsb_social_settings[youtube]',
        'label' => __('YouTube', 'hasbara'),
    ));

    /* COOKIES SETTINGS */
    $wp_customize->add_section('hsb_cookie_settings', array(
        'title'    => __('Cookies', 'hasbara'),
        'description' => __('Opciones de Cookies', 'hasbara'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('hsb_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'hasbara'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'hsb_cookie_settings',
        'settings' => 'hsb_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('hsb_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'hsb_cookie_settings',
        'settings' => 'hsb_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'hasbara'),
    ));
}

function hasbara_sanitize_url($url)
{
    return esc_url_raw($url);
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
function register_hasbara_settings() {
    register_setting( 'hasbara-settings-group', 'donation_percentage' );
    register_setting( 'hasbara-settings-group', 'donation_goal' );
}

add_action('admin_menu', 'hasbara_custom_panel_control');

function hasbara_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'hasbara' ),
        __( 'Panel de Control','hasbara' ),
        'edit_others_posts',
        'hasbara-control-panel',
        'hasbara_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_hasbara_settings' );
}

function hasbara_control_panel_callback() {
    ob_start();
?>
<div class="hasbara-admin-header-container">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="hasbara-admin-content-container">
    <?php settings_fields( 'hasbara-settings-group' ); ?>
    <?php do_settings_sections( 'hasbara-settings-group' ); ?>
    <div class="hasbara-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <td>
                    <label for="donation_percentage">Porcentaje de Donación: <input type="number" min="1" max="100" name="donation_percentage" value="<?php echo esc_attr( get_option('donation_percentage') ); ?>"></label>
                    <label for="donation_goal">Objetivo final de Donación: <input type="text" name="donation_goal" value="<?php echo esc_attr( get_option('donation_goal') ); ?>"></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="hasbara-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}

