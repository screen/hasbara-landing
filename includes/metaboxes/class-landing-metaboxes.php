<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Hasbara_Landing_Metaboxes')) :
    class Hasbara_Landing_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'hasbara_landing_custom_metabox'));
        }

        public function hasbara_landing_custom_metabox()
        {
            /* 1.- LANDING: HERO SECTION */
            $cmb_home_hero = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_hero_metabox',
                'title'         => esc_html__('Landing: Main Slider Hero', 'hasbara'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_image',
                'name'      => esc_html__('Image for Hero', 'hasbara'),
                'desc'      => esc_html__('Upload an image for this section', 'hasbara'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload Image', 'hasbara'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_intro',
                'name'      => esc_html__('Hero Title', 'hasbara'),
                'desc'      => esc_html__('Add a descriptive title for this Hero', 'hasbara'),
                'type'      => 'text'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_title',
                'name'      => esc_html__('Hero Title', 'hasbara'),
                'desc'      => esc_html__('Add a descriptive title for this Hero', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_desc',
                'name'      => esc_html__('Hero Description', 'hasbara'),
                'desc'      => esc_html__('Add a description below title for this Hero', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            /* 2.- LANDING: PRICES SECTION */
            $cmb_home_prices = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_prices_metabox',
                'title'         => esc_html__('Landing: Prices Section', 'hasbara'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));
            
            $cmb_home_prices->add_field(array(
                'id'        => parent::PREFIX . 'home_prices_title',
                'name'      => esc_html__('Section Title', 'hasbara'),
                'desc'      => esc_html__('Add a descriptive title for this Section', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_prices->add_field(array(
                'id'        => parent::PREFIX . 'home_prices_desc',
                'name'      => esc_html__('Section Description', 'hasbara'),
                'desc'      => esc_html__('Add a description below title for this Section', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            /*
            $cmb_home_prices->add_field(array(
                'id'        => parent::PREFIX . 'home_prices_actual',
                'name'      => esc_html__('Donation Actual', 'hasbara'),
                'desc'      => esc_html__('Add a donation goal', 'hasbara'),
                'type'      => 'text'
            ));
            */

            $cmb_home_prices->add_field(array(
                'id'        => parent::PREFIX . 'home_prices_percentage',
                'name'      => esc_html__('Donation Percentage', 'hasbara'),
                'desc'      => esc_html__('Add a donation percentage', 'hasbara'),
                'type'      => 'text',
                'attributes'      => array('type' => 'number', 'min' => 1, 'max' => 100)
            ));

            $cmb_home_prices->add_field(array(
                'id'        => parent::PREFIX . 'home_prices_goal',
                'name'      => esc_html__('Donation Goal', 'hasbara'),
                'desc'      => esc_html__('Add a donation goal', 'hasbara'),
                'type'      => 'text'
            ));

            $group_field_id = $cmb_home_prices->add_field(array(
                'id'            => parent::PREFIX . 'home_prices_group',
                'name'          => esc_html__('Prices Group', 'hasbara'),
                'description'   => __('Group of Prices inside this section', 'hasbara'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Price {#}', 'hasbara'),
                    'add_button'        => __('Add other Price', 'hasbara'),
                    'remove_button'     => __('Remove Price', 'hasbara'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Price?', 'hasbara')
                )
            ));

            $cmb_home_prices->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Item Title', 'hasbara'),
                'desc'      => esc_html__('Insert a title for this Item', 'hasbara'),
                'type'      => 'text'
            ));

            $cmb_home_prices->add_group_field($group_field_id, array(
                'id'        => 'desc',
                'name'      => esc_html__('Item Description', 'hasbara'),
                'desc'      => esc_html__('Insert a description for this Item', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_prices->add_group_field($group_field_id, array(
                'id'        => 'quantity',
                'name'      => esc_html__('Item Quantity', 'hasbara'),
                'desc'      => esc_html__('Insert a Quantity for this Item', 'hasbara'),
                'type'      => 'text'
            ));

            /* 3.- LANDING: BENEFITS SECTION */
            $cmb_home_benefits = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_benefits_metabox',
                'title'         => esc_html__('Landing: Benefits Section', 'hasbara'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_benefits->add_field(array(
                'id'        => parent::PREFIX . 'home_benefits_title',
                'name'      => esc_html__('Section Title', 'hasbara'),
                'desc'      => esc_html__('Add a descriptive title for this Section', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));
            
            $group_field_id = $cmb_home_benefits->add_field(array(
                'id'            => parent::PREFIX . 'home_benefits_group',
                'name'          => esc_html__('Benefits Group', 'hasbara'),
                'description'   => __('Group of Benefits inside this section', 'hasbara'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Benefit {#}', 'hasbara'),
                    'add_button'        => __('Add other Benefit', 'hasbara'),
                    'remove_button'     => __('Remove Benefit', 'hasbara'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Benefit?', 'hasbara')
                )
            ));

            $cmb_home_benefits->add_group_field($group_field_id, array(
                'id'        => 'image',
                'name'      => esc_html__('Benefit Image', 'hasbara'),
                'desc'      => esc_html__('Upload an image for this Benefit', 'hasbara'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload image', 'hasbara'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_benefits->add_group_field($group_field_id, array(
                'id'        => 'desc',
                'name'      => esc_html__('Benefit Description', 'hasbara'),
                'desc'      => esc_html__('Insert a description for this Benefit', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

             /* 3.- LANDING: TESTIMONIALS SECTION */
             $cmb_home_testimonials = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_test_metabox',
                'title'         => esc_html__('Landing: Testimonials Section', 'hasbara'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_testimonials->add_field(array(
                'id'        => parent::PREFIX . 'home_test_video',
                'name'      => esc_html__('Section Video', 'hasbara'),
                'desc'      => esc_html__('Add a descriptive Video for this Section', 'hasbara'),
                'type'      => 'textarea_code',
            ));
            
            $group_field_id = $cmb_home_testimonials->add_field(array(
                'id'            => parent::PREFIX . 'home_test_group',
                'name'          => esc_html__('Testimonials Group', 'hasbara'),
                'description'   => __('Group of Testimonial inside this section', 'hasbara'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Testimonial {#}', 'hasbara'),
                    'add_button'        => __('Add other Testimonial', 'hasbara'),
                    'remove_button'     => __('Remove Testimonial', 'hasbara'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Testimonial?', 'hasbara')
                )
            ));

            $cmb_home_testimonials->add_group_field($group_field_id, array(
                'id'        => 'desc',
                'name'      => esc_html__('Testimonial Description', 'hasbara'),
                'desc'      => esc_html__('Insert a description for this Testimonial', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_testimonials->add_group_field($group_field_id, array(
                'id'        => 'name',
                'name'      => esc_html__('Testimonial Name', 'hasbara'),
                'desc'      => esc_html__('Insert a Name for this Testimonial', 'hasbara'),
                'type'      => 'text',
            ));
            $cmb_home_testimonials->add_group_field($group_field_id, array(
                'id'        => 'position',
                'name'      => esc_html__('Testimonial Title', 'hasbara'),
                'desc'      => esc_html__('Insert a position for this Testimonial', 'hasbara'),
                'type'      => 'text',
            ));

            /* 4.- LANDING: ABOUT SECTION */
            $cmb_home_about = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_about_metabox',
                'title'         => esc_html__('Landing: About Section', 'hasbara'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_image',
                'name'      => esc_html__('Image for Hero', 'hasbara'),
                'desc'      => esc_html__('Upload an image for this Section', 'hasbara'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload Image', 'hasbara'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_title',
                'name'      => esc_html__('Section Title', 'hasbara'),
                'desc'      => esc_html__('Add a title for this Section', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_subtitle',
                'name'      => esc_html__('Section Subtitle', 'hasbara'),
                'desc'      => esc_html__('Add a subtitle for this Section', 'hasbara'),
                'type'      => 'text'
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_desc',
                'name'      => esc_html__('Section Description', 'hasbara'),
                'desc'      => esc_html__('Add a description below title for this Section', 'hasbara'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));
        }
    }
endif;

new Hasbara_Landing_Metaboxes;