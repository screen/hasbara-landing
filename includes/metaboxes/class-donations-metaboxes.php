<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Hasbara_Donation_Metaboxes')) :
    class Hasbara_Donation_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'hasbara_donation_custom_metabox'));
        }

        public function hasbara_donation_custom_metabox()
        {
            /* 1.- LANDING: HERO SECTION */
            $cmb_donations = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'donations_metabox',
                'title'         => esc_html__('Landing: Main Slider Hero', 'hasbara'),
                'object_types'  => array('donations'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_donations->add_field(array(
                'id'        => parent::PREFIX . 'donations_quantity',
                'name'      => esc_html__('Donation Quantity', 'hasbara'),
                'desc'      => esc_html__('Add the Donation Quantity in number format', 'hasbara'),
                'type'      => 'text',
                'attributes'      => array('type' => 'number')
            ));
        }
    }
endif;

new Hasbara_Donation_Metaboxes;