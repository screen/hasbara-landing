<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

// Register Custom Post Type
function donations_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Donaciones', 'Post Type General Name', 'hasbara' ),
		'singular_name'         => _x( 'Donación', 'Post Type Singular Name', 'hasbara' ),
		'menu_name'             => __( 'Donaciones', 'hasbara' ),
		'name_admin_bar'        => __( 'Donaciones', 'hasbara' ),
		'archives'              => __( 'Archivo de Donaciones', 'hasbara' ),
		'attributes'            => __( 'Atributos de Donación', 'hasbara' ),
		'parent_item_colon'     => __( 'Donación Padre:', 'hasbara' ),
		'all_items'             => __( 'Todas las Donaciones', 'hasbara' ),
		'add_new_item'          => __( 'Agregar nueva Donación', 'hasbara' ),
		'add_new'               => __( 'Agregar nueva', 'hasbara' ),
		'new_item'              => __( 'Nueva Donación', 'hasbara' ),
		'edit_item'             => __( 'Editar Donación', 'hasbara' ),
		'update_item'           => __( 'Actualizar Donación', 'hasbara' ),
		'view_item'             => __( 'Ver Donación', 'hasbara' ),
		'view_items'            => __( 'Ver Donaciones', 'hasbara' ),
		'search_items'          => __( 'Buscar Donaciones', 'hasbara' ),
		'not_found'             => __( 'No hay resultados', 'hasbara' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'hasbara' ),
		'featured_image'        => __( 'Imagen destacada', 'hasbara' ),
		'set_featured_image'    => __( 'Colocar Imagen destacada', 'hasbara' ),
		'remove_featured_image' => __( 'Remover Imagen destacada', 'hasbara' ),
		'use_featured_image'    => __( 'Usar como Imagen destacada', 'hasbara' ),
		'insert_into_item'      => __( 'Insertar en Donación', 'hasbara' ),
		'uploaded_to_this_item' => __( 'Cargado a esta Donación', 'hasbara' ),
		'items_list'            => __( 'Listado de Donaciones', 'hasbara' ),
		'items_list_navigation' => __( 'Navegación del Listado de Donaciones', 'hasbara' ),
		'filter_items_list'     => __( 'Filtro del Listado de Donaciones', 'hasbara' ),
	);
	$args = array(
		'label'                 => __( 'Donación', 'hasbara' ),
		'description'           => __( 'Donaciones', 'hasbara' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-money-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'donations', $args );

}
add_action( 'init', 'donations_custom_post_type', 0 );