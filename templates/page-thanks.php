<?php
/**
* Template Name: Pagina Gracias
*
* @package hasbara
* @subpackage hasbara-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section id="post-<?php the_ID(); ?>" class="page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="row">
                <div class="section-container landing-thanks-page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <svg class="thanks-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                        <circle class="path circle" fill="none" stroke="#78AD24" stroke-width="7" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"></circle>
                        <polyline class="path check" fill="none" stroke="#78AD24" stroke-width="7" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "></polyline>
                    </svg>
                    <?php the_content(); ?>
                    <a href="<?php echo home_url('/'); ?>" title="Volver al Inicio" class="btn btn-md btn-thanks-action">Volver al Inicio</a>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>