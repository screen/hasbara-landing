<?php
/**
* Template Name: Landing Template
*
* @package hasbara
* @subpackage hasbara-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="main-banner-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-banner-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-center">
                            <div class="main-banner-img col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <?php echo wp_get_attachment_image(get_post_meta(get_the_ID(), 'hsb_home_hero_image_id', true), 'full', array('class' => 'img-fluid'));?>
                            </div>
                            <div class="main-banner-text col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="intro">
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_hero_intro', true)); ?>
                                </div>
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_hero_title', true)); ?>
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_hero_desc', true)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section  id="donations" class="main-prices-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-prices-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_prices_title', true)); ?>
                    </div>
                    <div class="main-prices-desc col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_prices_desc', true)); ?>
                    </div>
                    <div class="progress-bar-container col-xl-9 col-lg-9 col-md-10 col-sm-12 col-12">
                        <div class="row align-items-end justify-content-between">
                            <?php $percentage = get_option('donation_percentage'); ?>
                            <?php $goal = get_option('donation_goal'); ?>
                            <div class="progress-item col-4">
                                <h4><?php _e('Recaudado', 'hasbara'); ?></h4>
                                <h3><?php echo $percentage; ?>%</h3>
                            </div>
                            <div class="progress-item progress-item-last col-4">
                                <h4><?php _e('Meta', 'hasbara'); ?></h4>
                                <h3><?php echo $goal; ?></h3>
                            </div>
                            <div class="col-12">
                                <div class="progress" style="height: 20px;">
                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $percentage ?>%;" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-12 date-update text-center">
                                <?php $date_month = date('F'); ?>
                                <?php switch ($date_month) {
                                    case 'January':
                                        $mes = 'Enero';
                                        break;

                                    case 'February':
                                        $mes = 'Febrero';
                                        break;
                                        
                                    case 'March':
                                        $mes = 'Marzo';
                                        break;
                                        
                                    case 'April':
                                        $mes = 'April';
                                        break;
                                    
                                    case 'May':
                                        $mes = 'Mayo';
                                        break;
                                                    
                                    case 'June':
                                        $mes = 'Junio';
                                        break;
                                    
                                    case 'July':
                                        $mes = 'Julio';
                                        break;

                                    case 'August':
                                        $mes = 'Agosto';
                                        break;
                                    
                                    case 'September':
                                        $mes = 'Septiembre';
                                        break;
                                                                    
                                    case 'October':
                                        $mes = 'Octubre';
                                        break;
                                    
                                    case 'November':
                                        $mes = 'Noviembre';
                                        break;
                                    
                                    case 'December':
                                        $mes = 'Diciembre';
                                        break;                                                                        

                                    default:
                                        # code...
                                        break;
                                }?>
                                <p><?php _e('Actualizado al', 'hasbara'); ?> <?php echo $mes . ' ' . date('d Y'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <?php $arr_donations = new WP_Query(array('post_type' => 'donations', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date')); ?>
                    <?php if ($arr_donations->have_posts()) : ?>
                    <?php while ($arr_donations->have_posts()) : $arr_donations->the_post(); ?>
                    <div class="main-prices-item col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="main-prices-item-wrapper">
                            <div class="text">
                                <h3><?php the_title(); ?></h3>
                                <div class="desc">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <div class="actions">
                                <a href="<?php echo home_url('/donar-ahora'); ?>/?donate_id=<?php echo get_the_ID(); ?>" class="btn btn-md btn-donate"><?php _e('Donar', 'hasbara'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </section>
        <section class="main-benefits-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-benefits-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_benefits_title', true)); ?>
                    </div>
                </div>
                <div class="row align-items-start justify-content-center">
                    <?php $benefits = get_post_meta(get_the_ID(), 'hsb_home_benefits_group', true); ?>
                    <?php if (!empty($benefits)) : ?>
                    <?php foreach ($benefits as $item) { ?>
                    <div class="main-benefits-item col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <?php echo wp_get_attachment_image($item['image_id'], 'full', false, array('class' => 'img-fluid')); ?>
                        <?php echo apply_filters('the_content', $item['desc']); ?>
                    </div>
                    <?php } ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="main-testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-testimonials-video col-xl-10 col-lg-10 col-md-11 col-sm-12 col-12">
                        <div class="ratio ratio-16x9">
                            <?php echo get_post_meta(get_the_ID(), 'hsb_home_test_video', true); ?>
                        </div>
                    </div>
                    <div class="main-testimonials-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php _e('Testimonios', 'hasbara'); ?></h2>
                    </div>
                    <div class="main-testimonials-slide-container col-xl-9 col-lg-9 col-md-10 col-sm-12 col-12">
                        <div class="swiper swiper-testimonials">
                            <div class="swiper-wrapper">
                                <?php $test = get_post_meta(get_the_ID(), 'hsb_home_test_group', true); ?>
                                <?php if (!empty($test)) : ?>
                                <?php foreach ($test as $item) { ?>
                                <div class="swiper-slide">
                                    <div class="testimonial-item-wrapper">
                                        <div class="desc">
                                            <?php echo apply_filters('the_content', $item['desc']); ?>
                                        </div>
                                        <div class="title">
                                            <h3><?php echo $item['name']; ?></h3>
                                            <?php echo apply_filters('the_content', $item['position']); ?>
                                        </div>
                                        <div class="star-container">
                                            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php endif; ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-banner-container main-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-banner-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row align-items-center">
                            <div class="main-banner-img col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <?php echo wp_get_attachment_image(get_post_meta(get_the_ID(), 'hsb_home_about_image_id', true), 'full', array('class' => 'img-fluid'));?>
                            </div>
                            <div class="main-banner-text col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_about_title', true)); ?>
                                <div class="intro">
                                    <?php echo get_post_meta(get_the_ID(), 'hsb_home_about_subtitle', true); ?>
                                </div>
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hsb_home_about_desc', true)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>